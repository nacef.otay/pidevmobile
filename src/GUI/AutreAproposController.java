/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entity.CentreInteret;

import Entity.User;
import com.codename1.components.SpanLabel;
import com.codename1.main.Controller;
import static com.codename1.main.Controller.theme;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import service.AutreAproposService;

/**
 *
 * @author Nacef
 */
public class AutreAproposController extends Controller {
    
    AutreAproposService autreAproposService = new AutreAproposService();
    
    public static User autreUser;

    public static User getAutreUser() {
        return autreUser;
    }

    public static void setAutreUser(User autreUser) {
        AutreAproposController.autreUser = autreUser;
    }
    
    
    public AutreAproposController()
    {
        super();
    }

    @Override
    public void initialize() {
        
        Container c = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Font normalSmall = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font normalMedium = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_MEDIUM);
        Font boldSmall = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL);
        Font boldLarge = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);

        ArrayList<CentreInteret> films = autreAproposService.getCentres(autreUser.getId().toString()).get("films");
        ArrayList<CentreInteret> series = autreAproposService.getCentres(autreUser.getId().toString()).get("series");
        ArrayList<CentreInteret> artists = autreAproposService.getCentres(autreUser.getId().toString()).get("artists");
        ArrayList<CentreInteret> livres = autreAproposService.getCentres(autreUser.getId().toString()).get("livres");
        
        //----------------------------------------------------------------------------------
        Label sep4 = new Label("\u0000"){
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        c.add(sep4);
        
        
        Container infos = new Container(new BoxLayout(BoxLayout.Y_AXIS));                
        
        
        Container inf1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n1 = new Label("Description: ");
        n1.getUnselectedStyle().setFont(normalMedium);
        SpanLabel description = new SpanLabel(autreUser.getApropos());
        description.getUnselectedStyle().setFont(normalMedium);
        inf1.add(n1).add(description);
        
        
        Container inf2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n2 = new Label("Date de naissance: ");
        n2.getUnselectedStyle().setFont(normalMedium);
        Label dn = new Label(df.format(autreUser.getDate_naissance()));
        dn.getUnselectedStyle().setFont(normalMedium);
        inf2.add(n2).add(dn);
        
        
        Container inf3 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n3 = new Label("Lieu de naissance: ");
        n3.getUnselectedStyle().setFont(normalMedium);
        Label placenaiss = new Label(autreUser.getPlaceNaiss());
        placenaiss.getUnselectedStyle().setFont(normalMedium);
        inf3.add(n3).add(placenaiss);
        
        
        Container inf4 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n4 = new Label("Sexe: ");
        n4.getUnselectedStyle().setFont(normalMedium);
        Label sexe = new Label();
        if(autreUser.getGender().equals("M"))
            sexe.setText("Homme");
        else
            sexe.setText("Femme");
        sexe.getUnselectedStyle().setFont(normalMedium);
        inf4.add(n4).add(sexe);
        
        
        Container inf5 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n5 = new Label("Vit en: ");
        n5.getUnselectedStyle().setFont(normalMedium);
        Label ville = new Label(autreUser.getVille());
        ville.getUnselectedStyle().setFont(normalMedium);
        inf5.add(n5).add(ville);
        
        
        Container inf6 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n6 = new Label("E-mail: ");
        n6.getUnselectedStyle().setFont(normalMedium);
        Label mail = new Label(autreUser.getEmail());
        mail.getUnselectedStyle().setFont(normalMedium);
        inf6.add(n6).add(mail);
        
        
        Container inf7 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n7 = new Label("Téléphone: ");
        n7.getUnselectedStyle().setFont(normalMedium);
        Label tel = new Label(autreUser.getTel());
        tel.getUnselectedStyle().setFont(normalMedium);
        inf7.add(n7).add(tel);
        
        
        Container inf8 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n8 = new Label("Religion: ");
        n8.getUnselectedStyle().setFont(normalMedium);
        Label religion = new Label();
        switch (autreUser.getReligion()) {
            case "I":  religion.setText("Islam");
                     break;
            case "C":  religion.setText("Christianisme");
                     break;
            case "J":  religion.setText("Judaïsme");
                     break;
            case "B":  religion.setText("Bouddhisme");
                     break;
            case "H":  religion.setText("Hindouisme");
                     break;            
        }
        
        religion.getUnselectedStyle().setFont(normalMedium);
        inf8.add(n8).add(religion);
        
        
        Container inf9 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n9 = new Label("Dernière connexion: ");
        n9.getUnselectedStyle().setFont(normalMedium);        
        Label lastlogin = new Label(df.format(autreUser.getLastLogin()));
        lastlogin.getUnselectedStyle().setFont(normalMedium);
        inf9.add(n9).add(lastlogin);
        
        infos.add(inf1).add(inf2).add(inf3).add(inf4).add(inf5).add(inf6).add(inf7).add(inf8).add(inf9);
        
        //--------
        Container rsxBorder = new Container(new BorderLayout());
        Container rsx = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        
        
        Image facebook = theme.getImage("facebook.png").scaled(Display.getInstance().getDisplayWidth()-15, 50);
        Container x1 = new Container();
        Label l1 = new Label(facebook);
        l1.addPointerReleasedListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Form fbForm = new Form(new BorderLayout());  
                fbForm.setScrollableY(true);
                fbForm.getToolbar().addCommandToRightBar("Précédent", theme.getImage("back-command.png"), new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {                
                        Form f = rootContainer.getComponentForm();
                        f.show();
                    }
                });
                BrowserComponent browser = new BrowserComponent();
                browser.setAlwaysTensile(true);
                browser.setScrollVisible(false);
                browser.setScrollableX(true);
                browser.setScrollableY(true);
                browser.setTactileTouch(true);
                browser.setURL(autreUser.getFacebook());
                fbForm.add(BorderLayout.CENTER, browser);
                fbForm.show();
            }
        });
        x1.add(l1);
        x1.setLeadComponent(l1);
        
        
        Image twitter = theme.getImage("twitter.png").scaled(Display.getInstance().getDisplayWidth()-15, 50);
        Container x2 = new Container();
        Label l2 = new Label(twitter);
        l2.addPointerReleasedListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Form fbForm = new Form(new BorderLayout()); 
                fbForm.setScrollableY(true);                
                fbForm.getToolbar().addCommandToRightBar("Précédent", theme.getImage("back-command.png"), new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {                
                        Form f = rootContainer.getComponentForm();
                        f.show();
                    }
                });
                BrowserComponent browser = new BrowserComponent();
                browser.setAlwaysTensile(true);
                browser.setScrollVisible(false);
                browser.setScrollableX(true);
                browser.setScrollableY(true);
                browser.setTactileTouch(true);
                browser.setURL(autreUser.getTwitter());
                fbForm.add(BorderLayout.CENTER, browser);
                fbForm.show();
            }
        });
        x2.add(l2);
        x2.setLeadComponent(l2);
        
        
        Image instagram = theme.getImage("instagram.png").scaled(Display.getInstance().getDisplayWidth()-15, 50);
        Container x3 = new Container();
        Label l3 = new Label(instagram);
        l3.addPointerReleasedListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Form fbForm = new Form(new BorderLayout());
                fbForm.setScrollableY(true);
                fbForm.getToolbar().addCommandToRightBar("Précédent", theme.getImage("back-command.png"), new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {                
                        Form f = rootContainer.getComponentForm();
                        f.show();
                    }
                });
                BrowserComponent browser = new BrowserComponent();
                browser.setAlwaysTensile(true);
                browser.setScrollVisible(false);
                browser.setScrollableX(true);
                browser.setScrollableY(true);
                browser.setTactileTouch(true);
                browser.setURL(autreUser.getInstagram());
                fbForm.add(BorderLayout.CENTER, browser);
                fbForm.show();
            }
        });
        x3.add(l3);
        x3.setLeadComponent(l3);

        
        rsx.add(x1).add(x2).add(x3);
        rsxBorder.add(BorderLayout.CENTER,rsx);
        infos.add(rsxBorder);
        //---------
        
        c.add(infos);
        Label sep = new Label("\u0000"){
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        c.add(sep);
        
        
        
        //----------------------------------------------------------------------------------
        
        Label ci = new Label("Centres d'interet"){
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };                
        ci.getUnselectedStyle().setFont(boldLarge);        
        c.add(ci);
        
        //----------------
        Label filmLab = new Label("Films préférés:");                
        filmLab.getUnselectedStyle().setFont(boldSmall);
        filmLab.getAllStyles().setUnderline(true);
        c.add(filmLab);  
        for(CentreInteret loi:films){
            Label x = new Label(loi.getContenu());
            x.getUnselectedStyle().setFont(normalSmall);            
            c.add(x);
        }
        //----------------
        Label serieLab = new Label("Série de télévision préférées:");                
        serieLab.getUnselectedStyle().setFont(boldSmall);
        serieLab.getAllStyles().setUnderline(true);
        c.add(serieLab);  
        for(CentreInteret loi:series){
            Label x = new Label(loi.getContenu());
            x.getUnselectedStyle().setFont(normalSmall);            
            c.add(x);
        }
        //----------------
        Label artistLab = new Label("Groupes de musique/Artistes préférés:");                
        artistLab.getUnselectedStyle().setFont(boldSmall);
        artistLab.getAllStyles().setUnderline(true);
        c.add(artistLab);  
        for(CentreInteret loi:artists){
            Label x = new Label(loi.getContenu());
            x.getUnselectedStyle().setFont(normalSmall);            
            c.add(x);
        }
        //----------------
        Label livreLab = new Label("Livres préférés:");                
        livreLab.getUnselectedStyle().setFont(boldSmall);
        livreLab.getAllStyles().setUnderline(true);
        c.add(livreLab);  
        for(CentreInteret loi:livres){
            Label x = new Label(loi.getContenu());
            x.getUnselectedStyle().setFont(normalSmall);            
            c.add(x);
        }
        //----------------
       
        //----------------
        Label sep2 = new Label("\u0000") {
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
       
        
        //--------------
        this.rootContainer.removeAll();
        this.rootContainer.add(BorderLayout.NORTH, c);
        this.rootContainer.revalidate();
    }
    
}
