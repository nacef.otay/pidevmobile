/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entity.User;
import com.codename1.main.Controller;
import com.codename1.ui.Container;
import com.codename1.ui.Font;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import java.text.SimpleDateFormat;
import java.util.List;
import service.SuggestionService;
import service.UtilService;

/**
 *
 * @author Nacef
 */
public class SuggestionController extends Controller {
    
    SuggestionService suggestionService = new SuggestionService();
    UtilService utilService = UtilService.getInstance();
    
    public SuggestionController()
    {
        super();
    }

    @Override
    public void initialize() {
        
        Font normalSmall = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font normalMedium = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_MEDIUM);
        Font boldSmall = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL);
        Font boldLarge = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);

        Container c = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        
        List<User> users = suggestionService.getSugg();
        c.add(new Label("Profil suggérés"));
        for(User u:users){
            Container sugContainer = new Container(new BoxLayout(BoxLayout.X_AXIS));
            Image img = utilService.getImageProfilFromURL(u.getImage());
            Container npDate = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Label np = new Label(u.getPrenom()+" "+u.getNom());
            np.getUnselectedStyle().setFont(normalMedium);
            np.setUIID(u.getId().toString());
            np.addPointerPressedListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    Label idLabel = (Label) evt.getSource();
                    AutreProfilController autreProfilController = new AutreProfilController();
                    String autreId = idLabel.getUIID();
                    autreProfilController.setIdAutreUser(autreId);

                    autreProfilController.initialize();
                    c.removeAll();
                    Container x = rootContainer.getParent().getParent().getParent();
                    x.removeAll();
                    x.add(BorderLayout.NORTH, autreProfilController.getView());
                    x.revalidate();

                }
            });
            SimpleDateFormat formater = new SimpleDateFormat("DD-MM-yyyy");
            Label dateNaiss = new Label(formater.format(u.getDate_naissance()));
            dateNaiss.getUnselectedStyle().setFont(normalMedium);
            npDate.add(np).add(dateNaiss);
            npDate.setLeadComponent(np);
            sugContainer.add(img).add(npDate);
            sugContainer.setLeadComponent(npDate);
            c.add(sugContainer);
            Label l = new Label("\u0000") {
                public void paint(Graphics g) {
                    super.paint(g);
                    g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
                }
            };
            c.add(l);
        }
        //-----------
        this.rootContainer.removeAll();
        this.rootContainer.add(BorderLayout.NORTH, c);
        this.rootContainer.revalidate();
    }
    
}
