/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entity.CentreInteret;

import Entity.User;
import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.SpanLabel;
import com.codename1.main.Controller;
import com.codename1.main.Session;
import com.codename1.testing.DeviceRunner;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import service.AproposService;

/**
 *
 * @author Nacef
 */
public class AProposController extends Controller {
    
    Session session = Session.getInstance();
        
    User u = session.ConnectedUser;
    
    public AProposController()
    {
        super();
    }

    @Override
    public void initialize() {
        AproposService aproposService = new AproposService();
        
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Font normalSmall = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font normalMedium = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_MEDIUM);
        Font boldSmall = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL);
        Font boldLarge = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);
        
        Container c = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        //c.setScrollableY(true);
        
        Label infp = new Label("Informations personelles"){
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };                
        infp.getUnselectedStyle().setFont(boldLarge);
        c.add(infp);
        //-----------------------------------------------------------------------------       
        
        Container infos = new Container(new BoxLayout(BoxLayout.Y_AXIS));                
        
        
        Container inf1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n1 = new Label("Description: ");
        n1.getUnselectedStyle().setFont(normalMedium);
        SpanLabel description = new SpanLabel(u.getApropos());
        description.getUnselectedStyle().setFont(normalMedium);
        inf1.add(n1).add(description);
        
        System.out.println(u.getApropos());
        
        Container inf2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n2 = new Label("Date de naissance: ");
        n2.getUnselectedStyle().setFont(normalMedium);
        Label dn = new Label(df.format(u.getDate_naissance()));
        dn.getUnselectedStyle().setFont(normalMedium);
        inf2.add(n2).add(dn);
                System.out.println(u.getDate_naissance());

        
        Container inf3 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n3 = new Label("Lieu de naissance: ");
        n3.getUnselectedStyle().setFont(normalMedium);
        Label placenaiss = new Label(u.getPlaceNaiss());
        placenaiss.getUnselectedStyle().setFont(normalMedium);
        inf3.add(n3).add(placenaiss);
        
        
        Container inf4 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n4 = new Label("Sexe: ");
        n4.getUnselectedStyle().setFont(normalMedium);
        Label sexe = new Label();
        if(u.getGender().equals("M"))
            sexe.setText("Homme");
        else
            sexe.setText("Femme");
        sexe.getUnselectedStyle().setFont(normalMedium);
        inf4.add(n4).add(sexe);
        
        
        Container inf5 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n5 = new Label("Vit en: ");
        n5.getUnselectedStyle().setFont(normalMedium);
        Label ville = new Label(u.getVille());
        ville.getUnselectedStyle().setFont(normalMedium);
        inf5.add(n5).add(ville);
        
        
        Container inf6 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n6 = new Label("E-mail: ");
        n6.getUnselectedStyle().setFont(normalMedium);
        Label mail = new Label(u.getEmail());
        mail.getUnselectedStyle().setFont(normalMedium);
        inf6.add(n6).add(mail);
        
        
        Container inf7 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n7 = new Label("Téléphone: ");
        n7.getUnselectedStyle().setFont(normalMedium);
        Label tel = new Label(u.getTel());
        tel.getUnselectedStyle().setFont(normalMedium);
        inf7.add(n7).add(tel);
        
        
        Container inf8 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n8 = new Label("Religion: ");
        n8.getUnselectedStyle().setFont(normalMedium);
        Label religion = new Label();
        switch (u.getReligion()) {
            case "I":  religion.setText("Islam");
                     break;
            case "C":  religion.setText("Christianisme");
                     break;
            case "J":  religion.setText("Judaïsme");
                     break;
            case "B":  religion.setText("Bouddhisme");
                     break;
            case "H":  religion.setText("Hindouisme");
                     break;            
        }
        
        religion.getUnselectedStyle().setFont(normalMedium);
        inf8.add(n8).add(religion);
        
        
        Container inf9 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label n9 = new Label("Dernière connexion: ");
        n9.getUnselectedStyle().setFont(normalMedium);        
//        Label lastlogin = new Label(df.format(u.getLastLogin()));
      ////  lastlogin.getUnselectedStyle().setFont(normalMedium);
     ///   inf9.add(n9).add(lastlogin);
        
        infos.add(inf1).add(inf2).add(inf3).add(inf4).add(inf5).add(inf6).add(inf7).add(inf8).add(inf9);                
        
        c.add(infos);
        Label sep = new Label("\u0000"){
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        c.add(sep);
        
        //-----------------------------------------------------------------------------
        
        Label ci = new Label("Centres d'interet"){
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };                
        ci.getUnselectedStyle().setFont(boldLarge);
        c.add(ci);
        
        ArrayList<CentreInteret> films = aproposService.getCentres().get("films");
        ArrayList<CentreInteret> series = aproposService.getCentres().get("series");
        ArrayList<CentreInteret> artists = aproposService.getCentres().get("artists");
        ArrayList<CentreInteret> livres = aproposService.getCentres().get("livres");

        //----------------
        Label filmLab = new Label("Films préférés:");                
        filmLab.getUnselectedStyle().setFont(boldSmall);
        filmLab.getAllStyles().setUnderline(true);
        c.add(filmLab);  
        for(CentreInteret loi:films){
            Label x = new Label(loi.getContenu());
            x.getUnselectedStyle().setFont(normalSmall);            
            c.add(x);
        }
        //----------------
        Label serieLab = new Label("Série de télévision préférées:");                
        serieLab.getUnselectedStyle().setFont(boldSmall);
        serieLab.getAllStyles().setUnderline(true);
        c.add(serieLab);  
        for(CentreInteret loi:series){
            Label x = new Label(loi.getContenu());
            x.getUnselectedStyle().setFont(normalSmall);            
            c.add(x);
        }
        //----------------
        Label artistLab = new Label("Groupes de musique/Artistes préférés:");                
        artistLab.getUnselectedStyle().setFont(boldSmall);
        artistLab.getAllStyles().setUnderline(true);
        c.add(artistLab);  
        for(CentreInteret loi:artists){
            Label x = new Label(loi.getContenu());
            x.getUnselectedStyle().setFont(normalSmall);            
            c.add(x);
        }
        //----------------
        Label livreLab = new Label("Livres préférés:");                
        livreLab.getUnselectedStyle().setFont(boldSmall);
        livreLab.getAllStyles().setUnderline(true);
        c.add(livreLab);  
        for(CentreInteret loi:livres){
            Label x = new Label(loi.getContenu());
            x.getUnselectedStyle().setFont(normalSmall);            
            c.add(x);
        }
      
        Label sep2 = new Label("\u0000") {
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        c.add(sep2);
        
       
        Label sep3 = new Label("\u0000") {
            public void paint(Graphics g) {
                super.paint(g);
                g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
            }
        };
        c.add(sep3);
        
        
        this.rootContainer.removeAll();
        this.rootContainer.add(BorderLayout.NORTH, c);
        this.rootContainer.revalidate();
    }
    
}
