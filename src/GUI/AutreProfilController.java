/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entity.Publication;
import Entity.User;
import com.codename1.main.Controller;
import com.codename1.main.Session;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import java.util.List;
import service.AutreJournalService;
import service.UtilService;

/**
 *
 * @author Nacef
 */
public class AutreProfilController extends Controller {
    
    AutreJournalService autreJournalService = new AutreJournalService();
    UtilService utilService = UtilService.getInstance();
    /*public Form profilForm;

    public Form getProfilForm() {
        return profilForm;
    }

    public void setProfilForm(Form autreProfilForm) {
        this.profilForm = autreProfilForm;
    }*/
    
    public static String idAutreUser;

    public static String getIdAutreUser() {
        return idAutreUser;
    }

    public static void setIdAutreUser(String idAutreUser) {
        AutreProfilController.idAutreUser = idAutreUser;
    }
    
    public AutreProfilController()
    {
        super();
    }

    @Override
    public void initialize() {
        //Form autreProfilForm = new Form();
        //autreProfilForm.setLayout(new BorderLayout());
        //autreProfilForm.setToolbar(profilForm.getToolbar());
        //--------------------------------------------------------------
        
        //--------------------------------------------------------------

        User autreUser = autreJournalService.getAutreUser(idAutreUser);
        //List<Publication> pubs = autreJournalService.getAutrePubs(idAutreUser);
        
        Container c = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        c.setScrollableY(true);        
        
        Container changec = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        
        Container entete = new Container(new BoxLayout(BoxLayout.X_AXIS));
        //c.add(theme.getImage(t.getImg()));
        Image img = utilService.getImageProfilFromURL(autreUser.getImage());
        Label nt = new Label(autreUser.getNom()+" "+autreUser.getPrenom());
        Label signaler = new Label("Signaler");
        signaler.addPointerPressedListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Dialog d = new Dialog("Signaler");
                TextArea popupBody = new TextArea("", 3, 10);
                popupBody.setHint("écrire votre raison içi ...");
                popupBody.setUIID("PopupBody");
                popupBody.setEditable(true);
                d.setLayout(new BorderLayout());
                d.add(BorderLayout.NORTH, popupBody);
                Container butts = new Container(new BoxLayout(BoxLayout.X_AXIS));
                Button valider = new Button("Confirmer");
                Button annuler = new Button("Annuler");

                valider.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        //System.out.println(p.getId()+" - "+popupBody.getText());
                        autreJournalService.signaler(idAutreUser, popupBody.getText());
                        d.dispose();
                    }
                });
                annuler.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {                            
                        d.dispose();
                    }
                });
                butts.add(valider).add(annuler);
                d.add(BorderLayout.SOUTH, butts);
                d.show();
            }
        });
        entete.add(img).add(nt).add(signaler);
        //c.add(entete);        
        Button journal = new Button("Journal");
        journal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                AutreJournalController autreJournalController = new AutreJournalController();
                autreJournalController.setAutreUser(autreUser);
                changec.removeAll();
                autreJournalController.initialize();
                changec.addComponent(autreJournalController.getView());
                changec.revalidate();
            }
        });
        
        Button apropos = new Button("A Propos");
        apropos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                AutreAproposController autreAproposController = new AutreAproposController();
                autreAproposController.setAutreUser(autreUser);
                changec.removeAll();                
                autreAproposController.initialize();                
                changec.addComponent(autreAproposController.getView());
                changec.revalidate();
            }
        });
        
        Button album = new Button("Album");
        album.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                AutreAlbumController autreAlbumController = new AutreAlbumController();
                autreAlbumController.setAutreUser(autreUser);
                changec.removeAll();
                autreAlbumController.initialize();                
                changec.addComponent(autreAlbumController.getView());
                changec.revalidate();
            }
        });
        
        
        c.add(entete);
        c.add(journal);
        c.add(apropos);
        c.add(album);
        c.add(changec);
        //------------
        this.rootContainer.removeAll();
        this.rootContainer.add(BorderLayout.NORTH, c);
        this.rootContainer.revalidate();

        //-----
        //autreProfilForm.addComponent(BorderLayout.CENTER, this.getView());
        //autreProfilForm.add(this.rootContainer);
        //autreProfilForm.show();
    }
    
}
