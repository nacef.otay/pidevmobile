/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entity.Publication;
import Entity.User;
import com.codename1.components.SpanLabel;
import com.codename1.main.Controller;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Font;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import java.text.SimpleDateFormat;
import java.util.List;
import service.AutreJournalService;
import service.UtilService;

/**
 *
 * @author Nacef
 */
public class AutreJournalController extends Controller {
    
    AutreJournalService autreJournalService = new AutreJournalService();
    UtilService utilService = UtilService.getInstance();
    
    public static User autreUser;

    public static User getAutreUser() {
        return autreUser;
    }

    public static void setAutreUser(User idAutreUser) {
        AutreJournalController.autreUser = idAutreUser;
    }
        
    
    public AutreJournalController()
    {
        super();
    }

    @Override
    public void initialize() {
        Font normalSmall = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        Font boldMedium = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
        
        Container changec = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        List<Publication> pubs = autreJournalService.getAutrePubs(autreUser.getId().toString());
        //-------------
        for (Publication p:pubs)
        {
            Container pubCont = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            
            Container pubEntete = new Container(new BoxLayout(BoxLayout.X_AXIS));
            
            Image img = utilService.getImageProfilFromURL(autreUser.getImage());
            pubEntete.add(img);
            
            

            Label np = new Label(p.getIdUser().getNom()+" "+p.getIdUser().getPrenom());
            np.getUnselectedStyle().setFont(boldMedium);
            SpanLabel contenu = new SpanLabel(p.getContenu());
            contenu.getUnselectedStyle().setFont(normalSmall);
            
                        
            Container npDate = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            SimpleDateFormat formater = new SimpleDateFormat("EEEE, d MMM yyyy hh:mm");
            Label datePub = new Label(formater.format(p.getDatePublication()));
            datePub.getUnselectedStyle().setFont(normalSmall);
            npDate.add(np).add(datePub);
            pubEntete.add(npDate);
            
            
            pubCont.add(pubEntete);                      
            pubCont.add(contenu);

            
            Label l = new Label("\u0000") {
                public void paint(Graphics g) {
                    super.paint(g);
                    g.drawLine(getX(), getY() + getHeight() - 1, getX() + getWidth(), getY() + getHeight() - 1);
                }
            };
            pubCont.add(l);
            
            //c.add(pubCont);
            changec.add(pubCont);
            
            //c.add(changec);
            /*c.removeAll();
            c.add(entete);
            c.add(journal);
            c.add(apropos);
            c.add(album);
            c.add(sugg);
            c.add(changec);*/
        }
        //-------------------
        this.rootContainer.removeAll();
        this.rootContainer.add(BorderLayout.NORTH, changec);
        this.rootContainer.revalidate();
    }
    
}
