/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Entity.Publication;
import Entity.User;
import com.codename1.components.InfiniteProgress;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.main.Controller;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nacef
 */
public class AutreJournalService {
    
    public User getAutreUser(String id)
    {
        ConnectionRequest con = new ConnectionRequest();
        con.addArgument("idautreuser", id);
        con.setUrl(Controller.ip+"/Pi-Dev-Web/web/app_dev.php/mobile/getautreuser");
        User user = new User();
        con.addResponseListener((NetworkEvent evt) -> {
            try {
                JSONParser jsonp = new JSONParser();
                Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                Map<String, Object> mapUser =  list.get(0);
                
                user.setId((int)Float.parseFloat(mapUser.get("id").toString()));
                user.setNom(mapUser.get("nom").toString());
                user.setPrenom(mapUser.get("prenom").toString());                
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    user.setDate_naissance(format.parse(mapUser.get("dateNaissance").toString()));
                } catch (ParseException ex) {                           
                }
                user.setGender(mapUser.get("Gender").toString());
                user.setEmail(mapUser.get("email").toString());
                user.setPays(mapUser.get("pays").toString());
                user.setVille(mapUser.get("ville").toString());
                user.setRegion(mapUser.get("region").toString());
                user.setTel(mapUser.get("tel").toString());
                user.setPlaceNaiss(mapUser.get("placeNaiss").toString());
                user.setReligion(mapUser.get("relegion").toString());
                user.setApropos(mapUser.get("description").toString());
                user.setFacebook(mapUser.get("facebook").toString());
                user.setInstagram(mapUser.get("instagram").toString());
                user.setTwitter(mapUser.get("twitter").toString());
                try {
                    user.setLastLogin(format.parse(mapUser.get("lastLogin").toString()));
                } catch (ParseException ex) {                           
                }                
                user.setImage(mapUser.get("image").toString());
            } catch (IOException ex) {
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return user;
    }
    
    public ArrayList<Publication> getAutrePubs(String id) {
        ArrayList<Publication> listPubs = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.addArgument("idautreuser", id);
        con.setUrl(Controller.ip+"/Pi-Dev-Web/web/app_dev.php/mobile/getautrepubs");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    //renvoi une map avec clé = root et valeur le reste
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));

                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");

                    for (Map<String, Object> obj : list) {
                        Publication pub = new Publication();
                        float id = Float.parseFloat(obj.get("id").toString());
                        
                        pub.setId((int) id);
                        pub.setContenu(obj.get("contenu").toString());
                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            pub.setDatePublication(format.parse(obj.get("datePublication").toString()));
                        } catch (ParseException ex) {                           
                        }
                        User u = new User();
                        Map<String, Object> userMap = (Map<String, Object>) obj.get("user");
                        
                        u.setId((int)Float.parseFloat(userMap.get("id").toString()));
                        u.setNom(userMap.get("nom").toString());
                        u.setPrenom((String) userMap.get("prenom"));
                        pub.setIdUser(u);
                        listPubs.add(pub);

                    }
                } catch (IOException ex) {
                }

            }
        });
        InfiniteProgress prog = new InfiniteProgress();
        Dialog dlg = prog.showInifiniteBlocking();
        con.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listPubs;
    }
    
    public void signaler(String id, String contenu){
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(Controller.ip+"/Pi-Dev-Web/web/app_dev.php/mobile/signaler");
        con.addArgument("contenu", contenu);
        con.addArgument("idautreuser", id);
        con.setPost(true);
        InfiniteProgress prog = new InfiniteProgress();
        Dialog dlg = prog.showInifiniteBlocking();
        con.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
}
