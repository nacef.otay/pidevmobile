/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entity.User;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.main.Session;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ISLEM_PC
 */
public class loginServices {

    public void login(String username, String password) {

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://127.0.0.1/Pi-Dev-Web/web/app_dev.php/mobile/logiin/" + username + "/" + password);
        User user = new User();
        con.addResponseListener((NetworkEvent evt) -> {
            try {
                System.out.println("try catch ");
                JSONParser jsonp = new JSONParser();
                Map<String, Object> mapUser = (Map<String, Object>) jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));

                System.out.println("xD" + mapUser);
                //for (Map<String, Object> obj : list) {

                float id = Float.parseFloat(mapUser.get("id").toString());
                user.setId((int) id);
                if ((int) id > 0) {
                    Session.ConnectedUser.setPassword(password);
                    try {
                        Session.ConnectedUser.setId((int) Float.parseFloat(mapUser.get("id").toString()));
                        Session.ConnectedUser.setUsername(mapUser.get("username").toString());
                        Session.ConnectedUser.setRoles(mapUser.get("roles").toString());
                        Session.ConnectedUser.setNom(mapUser.get("nom").toString());
                        Session.ConnectedUser.setPrenom(mapUser.get("prenom").toString());
                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                        try {
                            Session.ConnectedUser.setDate_naissance(format.parse(mapUser.get("dateNaissance").toString()));
                        } catch (ParseException ex) {
                        }
                        Session.ConnectedUser.setImage(mapUser.get("image").toString());
                        Session.ConnectedUser.setGender(mapUser.get("Gender").toString());
                        Session.ConnectedUser.setPays(mapUser.get("pays").toString());
                        Session.ConnectedUser.setRegion(mapUser.get("region").toString());

                        Session.ConnectedUser.setEmail(mapUser.get("email").toString());
                        Session.ConnectedUser.setTel(mapUser.get("tel").toString());
                        Session.ConnectedUser.setVille(mapUser.get("ville").toString());
                        Session.ConnectedUser.setPlaceNaiss(mapUser.get("placeNaiss").toString());
                        Session.ConnectedUser.setReligion(mapUser.get("relegion").toString());
                        Session.ConnectedUser.setApropos(mapUser.get("apropos").toString());
                        try {
                            Session.ConnectedUser.setLastLogin(format.parse(mapUser.get("lastLogin").toString()));
                        } catch (ParseException ex) {

                            if (mapUser.get("facebook") != null) {
                                Session.ConnectedUser.setFacebook(mapUser.get("facebook").toString());
                            }
                            if (mapUser.get("twitter") != null) {
                                Session.ConnectedUser.setTwitter(mapUser.get("twitter").toString());
                            }
                            if (mapUser.get("instagram") != null) {
                                Session.ConnectedUser.setInstagram(mapUser.get("instagram").toString());
                            }
                        }

                    } catch (NullPointerException n) {

                    }
                }

                //}
            } catch (IOException ex) {
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        System.out.println("Login Serviceesss" + Session.ConnectedUser);
                System.out.println("Pays" + Session.ConnectedUser.getPays());
                System.out.println("Pays" + Session.ConnectedUser.getRegion());


    }
    
    

}
