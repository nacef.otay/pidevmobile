/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Entity.User;
import com.codename1.components.InfiniteProgress;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.main.Controller;
import com.codename1.main.Session;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nacef
 */
public class ParamsProfilService {
    
   
    
    public void modifierUserImage(String image){
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(Controller.ip+"/Pi-Dev-Web/web/app_dev.php/mobile/modifieruserimage");
        con.addArgument("user", Session.ConnectedUser.getId().toString());
        con.addArgument("imageurl", image);
        con.setPost(true);
        InfiniteProgress prog = new InfiniteProgress();
        Dialog dlg = prog.showInifiniteBlocking();
        con.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public void modifierUser(User user){
        ConnectionRequest con = new ConnectionRequest();
             con.addArgument("user",Session.ConnectedUser.getId().toString());
           
        con.addArgument("nom", user.getNom());
        con.addArgument("prenom", user.getPrenom());
        con.addArgument("sexe", user.getGender());
        con.addArgument("datenaiss", user.getDate_naissance().toString());
        //con.addArgument("pays", user.getImage());
        //con.addArgument("ville", user.getImage());
        //con.addArgument("region", user.getImage());
        con.addArgument("tel", user.getTel());
        con.addArgument("placenaiss", user.getPlaceNaiss());
        con.addArgument("religion", user.getReligion());
        con.addArgument("apropos", user.getApropos());
        con.addArgument("facebook", user.getFacebook());
        con.addArgument("twitter", user.getTwitter());
        con.addArgument("instagram", user.getInstagram());
        
        con.setUrl(Controller.ip+"/Pi-Dev-Web/web/app_dev.php/mobile/modifieruser");
        con.setPost(true);
        InfiniteProgress prog = new InfiniteProgress();
        Dialog dlg = prog.showInifiniteBlocking();
        con.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    
    public ArrayList<String> getPays() {
        ArrayList<String> listPays = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(Controller.ip+"/PiMobile/src/APIs/pays.json");
        con.addResponseListener((NetworkEvent evt) -> {
            //listTasks = getListTask(new String(con.getResponseData()));
            JSONParser jsonp = new JSONParser();
            
            try {
                //renvoi une map avec clé = root et valeur le reste
                Map<String, Object> data = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                
                List<Map<String, Object>>  list =  (List<Map<String, Object>>) data.get("root");
                
                int i = 0;
                for (Map<String, Object> obj:list) {
                    listPays.add(obj.get("name").toString()+"-"+obj.get("code").toString());
                }
            } catch (IOException ex) {
            }
        });
        InfiniteProgress prog = new InfiniteProgress();
        Dialog dlg = prog.showInifiniteBlocking();
        con.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listPays;
    }
    
    public ArrayList<String> getVille() {
        ArrayList<String> listPays = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(Controller.ip+"/PiMobile/src/APIs/ville.json");
        con.addResponseListener((NetworkEvent evt) -> {
            //listTasks = getListTask(new String(con.getResponseData()));
            JSONParser jsonp = new JSONParser();
            
            try {
                //renvoi une map avec clé = root et valeur le reste
                Map<String, Object> data = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                
                List<Map<String, Object>>  list =  (List<Map<String, Object>>) data.get("root");
                
                int i = 0;
                for (Map<String, Object> obj:list) {
                    listPays.add(obj.get("region").toString());
                }
            } catch (IOException ex) {
            }
        });
        InfiniteProgress prog = new InfiniteProgress();
        Dialog dlg = prog.showInifiniteBlocking();
        con.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listPays;
    }
}
