/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Entity.Evenement;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.main.Controller;
import com.codename1.main.Session;

import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author DJAZIA
 */
public class EvenementService {

    public ArrayList<Evenement> getList() {
        ArrayList<Evenement> listTasks = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(Controller.ip + "/pi-dev-web/web/app_dev.php/eventApi/");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                try {
                    //renvoi une map avec clé = root et valeur le reste
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                    System.out.println("list : " + list);
                    for (Map<String, Object> obj : list) {
                        Evenement task = new Evenement();
                        Double x = Double.parseDouble(obj.get("id").toString());
                        task.setId(x.intValue());
                        task.setTitre(obj.get("nomEvenement").toString());
                        task.setImageEve(obj.get("image").toString());
                        Double y = Double.parseDouble(obj.get("nbreplace").toString());
                        task.setNbplaces(y.intValue());
                        listTasks.add(task);
                    }
                } catch (IOException ex) {
                }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listTasks;
    }

    public ArrayList<Evenement> getList2(int id) {
        ArrayList<Evenement> listTasks = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(Controller.ip + "/pi-dev-web/web/app_dev.php/eventApi/detail/" + id);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                try {
                    //renvoi une map avec clé = root et valeur le reste
                    Map<String, Object> eventMap = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));

                    Evenement event = new Evenement();
                    event.setTitre(eventMap.get("nomEvenement").toString());
                    event.setImageEve(eventMap.get("image").toString());
                    event.setDescription(eventMap.get("description").toString());
                    event.setTitreCordination(eventMap.get("nomEvenement").toString());
                    Double x = (double) eventMap.get("nbreplace");

                    event.setNbplaces(x.intValue());

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
                    Date dateEvenement = format.parse(eventMap.get("dateDebut").toString());

                    event.setDateEvenement(dateEvenement);
                    listTasks.add(event);
                } catch (IOException ex) {
                } catch (ParseException ex) {
                }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listTasks;
    }

    public static boolean partEvent(Evenement e) {

        String url = Controller.ip + "/pi-dev-web/web/app_dev.php/eventApi/getp/" + e.getId();
        ConnectionRequest cr = new ConnectionRequest(url);
        cr.setPost(false);
        NetworkManager.getInstance().addToQueue(cr);
        return true;
    }
    
    public boolean checkParticipation(int id)
    {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(Controller.ip + "/pi-dev-web/web/app_dev.php/eventApi/checkparticipation");
        con.addArgument("ide", String.valueOf(id));
        con.addArgument("user", Session.ConnectedUser.getId().toString());
        NetworkManager.getInstance().addToQueueAndWait(con);
        String response = new String(con.getResponseData());
        System.out.println(response);
        if(response.equals("\"OK\"")) return true;
        return false;
    }
    
    public void participer(int id)
    {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(Controller.ip + "/pi-dev-web/web/app_dev.php/eventApi/participation");
        con.addArgument("user", Session.ConnectedUser.getId().toString());
        con.addArgument("event",String.valueOf(id));
        NetworkManager.getInstance().addToQueueAndWait(con);
        con.getResponseData();
    }
    
}
